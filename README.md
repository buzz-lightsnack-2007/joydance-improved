# JoyDance
![image](https://user-images.githubusercontent.com/96280/163298419-6279f338-069e-4302-971f-b9d2e5fc9f7a.png)

## Features
Play Just Dance 2016 and later on all platforms with Joy-Cons!

- 6 players supported
- no latency
- no random disconnection

### Compatiblitiy
|              | Xbox Series | Xbox One | PS4/5 | Nintendo Switch | Stadia | PC | Wii U |
|--------------|:-----------:|:--------:|:-----:|:---------------:|:------:|:--:|:-----:|
| 2020-2022    | √           | √.       | √     | √               | √      |    |       |
| 2016-2019 ⚠︎  |             | √        | √     | √               |        | √  | √     |

**Important**: Can't use buttons on Joy-Con to navigate the UI in JD 2016-2019 (you'll have to use controllers/keyboard). See [#6](../../issues/6).

### Benefits
- Playing with a Joy-Con, with the strap on, is safer and more comfortable than holding a phone.
- Score better than with a phone! 

## How does it work?
It pretends to be the [Just Dance Controller app](https://urlzs.com/DuxZQ), sending movements of the Joy-Con to your game console.

## Requirements
### Hardware
- Bluetooth module or dongle
	- It is preferred to use a Bluetooth dongle because built-in Bluetooth may not perform quite well. Make sure you buy a dongle with game controllers support, not just for audio devices. Not all dongles support macOS or Linux, so remember to check compatibility before buying.
- up to 6 Joy-Cons
	- It's recommended to update the Joy-Cons to the latest firmware and to calibate its motion sensors. 

### Software
- a desktop operating system
	- ChromeOS is [not supported](https://urlzs.com/svS9G). 
- [Python 3.7+](https://www.python.org) and [`pip`](https://pip.pypa.io/en/stable/installation/) installed

## Installation

1. Download [the latest version](https://gitdab.com/buzz-lightsnack-2007/joydance-improved/releases/) and extract it into a folder.
	- The dependencies will be automatically downloaded. _(new from original version)_
	
### Extra steps for Linux users
<details>
	<summary>Click to expand!</summary>

	1. Linux users may need to use [`hid`](https://github.com/apmorton/pyhidapi) instead of [`hidapi`](https://github.com/trezor/cython-hidapi) (not sure why `hidapi` couldn't find Joy-Cons on Linux).
	```
	pip3 uninstall hidapi
	sudo apt install libhidapi-dev
	pip3 install hid
	```

	2. Create a new udev rule file at `/etc/udev/rules.d/50-nintendo-switch.rules` ([Source](https://www.reddit.com/r/Stadia/comments/egcvpq/using_nintendo_switch_pro_controller_on_linux/fc5s7qm/))
	```
	# Switch Joy-con (L) (Bluetooth only)
	KERNEL=="hidraw*", SUBSYSTEM=="hidraw", KERNELS=="0005:057E:2006.*", MODE="0666"

	# Switch Joy-con (R) (Bluetooth only)
	KERNEL=="hidraw*", SUBSYSTEM=="hidraw", KERNELS=="0005:057E:2007.*", MODE="0666"

	# Switch Pro controller (USB and Bluetooth)
	KERNEL=="hidraw*", SUBSYSTEM=="hidraw", ATTRS{idVendor}=="057e", ATTRS{idProduct}=="2009", MODE="0666"
	KERNEL=="hidraw*", SUBSYSTEM=="hidraw", KERNELS=="0005:057E:2009.*", MODE="0666"

	# Switch Joy-con charging grip (USB only)
	KERNEL=="hidraw*", SUBSYSTEM=="hidraw", ATTRS{idVendor}=="057e", ATTRS{idProduct}=="200e", MODE="0666"
	```

	Reload udev rules:
	```
	sudo udevadm control --reload-rules
	sudo udevadm trigger
	```

	3. Install [`dkms-hid-nintendo`](https://github.com/nicman23/dkms-hid-nintendo) (Joy-Con driver) if you're running Linux kernel older than 5.16.
</details>

## Usage
1. Open the phone pairing screen on Just Dance.
2. Connect your computer to the same Wi-Fi or LAN network as your game console.
	- For Stadia, you just need to connect to the internet.
3. Run the executable or the Python script (`dance.py`). 
4. Open [http://localhost:32623](http://localhost:32623) in a web browser. You can also open JoyDance on another device connected to the same network by replacing `localhost` with your computer's private IP address. 
5. Turn on Bluetooth, and pair with the Joy-Cons by holding down the [sync button](https://en-americas-support.nintendo.com/app/answers/detail/a_id/22634) until the light move up and down. Press the "Refresh" button until your Joy-Con shows up.
6. Fill the form.
	**Pairing Method**:
		
	|                    | Default        | Fast      | Stadia    | Old                   |
	|--------------------|----------------|-----------|-----------|-----------------------|
	| Platforms          | All non-Stadia | Xbox One, PlayStation, Nintendo Switch | Stadia    | all |
	| Game versions      | 2020-2022      | 2020-2022 | 2020-2022 | 2016-2019             |
	| Pairing Speed      | slow           | fast      | slow      | fast                  |
	| Pairing Code       | √              |           | √         |                       |
	| Private IP Address | host           | console   |           | PC, console           |

	- **Host's Private IP Address**:
		- The private IP address of your device running JoyDance. Find this in the Wi-Fi settings.
		- Starts with `192.168.` or `10.`.
	- **PC/Console's Private IP Address**:
		- The private IP address of your PC or gaming console. Find this in the Wi-Fi settings on that device. 
		- Starts with `192.168.` or `10.`.
	- **Pairing Code**: Get this from the game.
7. Press the "Connect" button next to Joy-Con to start the pairing process.
8. Once paired, go ahead with the game! 

### Controller Mappings
| Joy-Con (L) | Joy-Con (R) |
|-------------|-------------|
| Up          | X           |
| Right       | A           |
| Down        | B           |
| Left        | Y           |
| L           | R           |
| ZL          | ZR          |
| Minus       | Plus        |

## FAQs
### 1. What is the correct way to hold a Joy-Con?
Please follow these images for [Joy-Con (L)](https://urlzs.com/NbKf7) and [Joy-Con (R)](https://urlzs.com/1BG22).  
### 2. How to exit JoyDance?
Close the only window, or press `⌃C`. 

### 3. Can I play Just Dance 2017 on a PC while using JoyDance?
Yes, you can.

## Acknowledgements
- [x] [redphx/joydance](https://github.com/redphx/joydance)
- [x] [dekuNukem/Nintendo_Switch_Reverse_Engineering](https://github.com/dekuNukem/Nintendo_Switch_Reverse_Engineering)
- [x] [tocoteron/joycon-python](https://github.com/tocoteron/joycon-python)
- [x] [Davidobot/BetterJoy](https://github.com/Davidobot/BetterJoy)
